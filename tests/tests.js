/******************************************************************************
* Test Code
* WARNING: Horrible code follows.
**************************************************************************** */

function TestClass(value) {
    this.value = value;
}
TestClass.proto_define = function(name, func) {
    TestClass.prototype[name] = func;
    return TestClass;
};

TestClass.proto_define('toString', function() {
        return '<TestClass('+this.value+')>';
    }).proto_define('__eq__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return eq(this.value, other);
        }
        if (other.constructor == this.constructor) {
            return eq(this.value, other.value);
        }
        return undefined;
    })
    .proto_define('__neq__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return neq(this.value, other);
        }
        if (other.constructor == this.constructor) {
            return neq(this.value, other.value);
        }
        return undefined;
    })
    .proto_define('__req__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return req(this.value, other);
        }
        if (other.constructor == this.constructor) {
            return req(this.value, other.value);
        }
        return undefined;
    })
    .proto_define('__rneq__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return rneq(this.value, other);
        }
        if (other.constructor == this.constructor) {
            return rneq(this.value, other.value);
        }
        return undefined;
    }).proto_define('__gt__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return gt(this.value, other);
        }
        if (other.constructor == TestClass) {
            return gt(this.value, other.value);
        }
        return undefined;
    }).proto_define('__rgt__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return rgt(this.value, other);
        }
        if (other.constructor == TestClass) {
            return rgt(this.value, other.value);
        }
        return undefined;
    }).proto_define('__gte__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return gte(this.value, other);
        }
        if (other.constructor == TestClass) {
            return gte(this.value, other.value);
        }
        return undefined;
    }).proto_define('__rgte__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return rgte(this.value, other);
        }
        if (other.constructor == TestClass) {
            return rgte(this.value, other.value);
        }
        return undefined;
    }).proto_define('__div__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass( div(this.value, other) )
        }
        if (other.constructor == this.constructor) {
            return new TestClass( div(this.value, other.value) )
        }
        return undefined;
    }).proto_define('__rdiv__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(rdiv(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( rdiv(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__mult__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(mult(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( mult(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__rmult__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(rmult(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( rmult(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__add__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(add(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( add(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__radd__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(radd(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( radd(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__sub__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(sub(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( sub(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__rsub__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(rsub(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( rsub(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__pow__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(pow(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( pow(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__rpow__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(rpow(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( rpow(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__mod__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(mod(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( mod(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__rmod__', function(other) {
        if (typeof(other) == typeof(this.value)) {
            return new TestClass(rmod(this.value, other));
        }
        if (other.constructor == TestClass) {
            return new TestClass( rmod(this.value, other.value) );
        }
        return undefined;
    }).proto_define('__neg__', function() {
        return new TestClass( neg(this.value) );
    }).proto_define('__pos__', function() {
        return new TestClass( pos(this.value) );
    }).proto_define('__bool__', function() {
        return neq(this.value, 0) && neq(this.value, null);
    }).proto_define('__iter__', function() {
        return iter([this.value]);
    });



function get_table() {
    return document.getElementById('results_table');
};
function get_time() {
    return (new Date()).getTime();
};

function get_test_name(test) {
    if (test == eq) { return 'Equal'; }
    if (test == req) { return 'Reverse Equal'; }
    if (test == neq) { return 'Not Equal'; }
    if (test == rneq) { return 'Reverse Not Equal'; }
    if (test == div) { return 'Divide'; }
    if (test == rdiv) { return 'Reverse Divide'; }
    if (test == mult) { return 'Multiply'; }
    if (test == rmult) { return 'Reverse Multiply'; }
    if (test == add) { return 'Add'; }
    if (test == radd) { return 'Reverse Add'; }
    if (test == sub) { return 'Subtract'; }
    if (test == rsub) { return 'Reverse Subtract'; }
    if (test == pow) { return 'Power'; }
    if (test == rpow) { return 'Reverse  Power'; }
    if (test == mod) { return 'Modulo'; }
    if (test == rmod) { return 'Reverse Modulo'; }
    if (test == gt) { return 'Greater Than'; }
    if (test == gte) { return 'Greater Than or Equal to'; }
    if (test == lt) { return 'Less Than'; }
    if (test == lte) { return 'Less Than or Equal to'; }
    if (test == bool) { return 'Boolean'; }
    if (test == iter) { return 'Iterable'; }
};

function test(description, chi, gamma, test, expectation) {
    var start = get_time();
    var result = undefined;
    try {
        result = test(chi, gamma);
    } catch(err) {
        result = err;
    }
    var delta = get_time()-start;
    var passed = eq(result, expectation);
    var rows = [
        description, chi, gamma, get_test_name(test), expectation, 
        typeof(result), result, delta
    ];
    var tr = document.createElement('tr');
    var td = null;

    for (var i=0; i<rows.length; i++) {
        td = document.createElement('td');
        td.innerText = rows[i];
        tr.appendChild(td);
    }
    if (passed) {
        tr.setAttribute('class', 'passed');
    } else {
        tr.setAttribute('class', 'failed');
    }
    get_table().appendChild(tr);
}

function t(x){ return new TestClass(x); }

window.onload = function() {
    // Equal Tests
    test('2 Numbers Equal', 5, 5, eq, true);
    test('2 Numbers Unequal', 5, 7, eq, false);
    test('Equivalent objects 1', t(5), 5, eq, true);
    test('Equivalent objects 2', 5, t(5), eq, true);
    test('Equivalent objects 3', t(5), t(5), eq, true);
    test('Equivalent objects 4', true, t(true), eq, true);
    test('Equivalent objects 4', t(true), true, eq, true);
    test('Equivalent objects 4', t(true), t(true), eq, true);
    test('Equivalent objects 5', false, t(false), eq, true);
    test('Equivalent objects 5', t(false), false, eq, true);
    test('Equivalent objects 5', t(false), t(false), eq, true);
    test('Different objects 1', t(5), 7, eq, false);
    test('Different objects 2', 5, t(7), eq, false);
    test('Different objects 3', t(5), t(7), eq, false);
    test('Different objects 3', t(true), t(false), eq, false);
    test('Equal Value', t(5.0), t(5), eq, true);

    // Not Equal tests
    test('2 Numbers Equal', 5, 5, neq, false);
    test('2 Numbers Unequal', 5, 7, neq, true);
    test('Equivalent objects 1', t(5), 5, neq, false);
    test('Equivalent objects 2', 5, t(5), neq, false);
    test('Equivalent objects 3', t(5), t(5), neq, false);
    test('Different objects 1', t(5), 7, neq, true);
    test('Different objects 2', 5, t(7), neq, true);
    test('Different objects 3', t(5), t(7), neq, true);
    test('Equal Value', t(5.0), t(5), neq, false);


    // Greater Than tests
    test('2 Numbers Equal', 5, 5, gt, false);
    test('2 Numbers Unequal', 5, 7, gt, false);
    test('Equivalent objects 1', t(5), 5, gt, false);
    test('Equivalent objects 2', 5, t(5), gt, false);
    test('Equivalent objects 3', t(5), t(5), gt, false);
    test('Different objects 1', t(5), 7, gt, false);
    test('Different objects 2', 5, t(7), gt, false);
    test('Different objects 3', t(7), t(5), gt, true);
    test('Equal Value', t(5.0), t(5), gt, false);

    // Greater Than or Equal to tests
    test('2 Numbers Equal', 5, 5, gte, true);
    test('2 Numbers Unequal', 5, 7, gte, false);
    test('Equivalent objects 1', t(5), 5, gte, true);
    test('Equivalent objects 2', 5, t(5), gte, true);
    test('Equivalent objects 3', t(5), t(5), gte, true);
    test('Different objects 1', t(5), 7, gte, false);
    test('Different objects 2', 5, t(7), gte, false);
    test('Different objects 3', t(7), t(5), gte, true);
    test('Equal Value', t(5.0), t(5), gte, true);

    // Less Than tests
    test('2 Numbers Equal', 5, 5, lt, false);
    test('2 Numbers Unequal', 5, 7, lt, true);
    test('Equivalent objects 1', t(5), 5, lt, false);
    test('Equivalent objects 2', 5, t(5), lt, false);
    test('Equivalent objects 3', t(5), t(5), lt, false);
    test('Different objects 1', t(5), 7, lt, true);
    test('Different objects 2', 5, t(7), lt, true);
    test('Different objects 3', t(7), t(5), lt, false);
    test('Equal Value', t(5.0), t(5), lt, false);

    // Less Than or Equal to tests
    test('2 Numbers Equal', 5, 5, lte, true);
    test('2 Numbers Unequal', 5, 7, lte, true);
    test('Equivalent objects 1', t(5), 5, lte, true);
    test('Equivalent objects 2', 5, t(5), lte, true);
    test('Equivalent objects 3', t(5), t(5), lte, true);
    test('Different objects 1', t(5), 7, lte, true);
    test('Different objects 2', 5, t(7), lte, true);
    test('Different objects 3', t(7), t(5), lte, false);
    test('Equal Value', t(5.0), t(5), lte, true);

    // Division Tests
    test('Simple Division', 10, 2, div, 5);
    test('Object First', t(10), 2, div, 5);
    test('Object Second', 10, t(2), div, 5);
    test('Both Objects', t(10), t(2), div, 5);

    // Multiplication Tests
    test('Simple Multiplication', 10, 2, mult, 20);
    test('Object First', t(10), 2, mult, 20);
    test('Object Second', 10, t(2), mult, 20);
    test('Both Objects', t(10), t(2), mult, 20);

    // Addition Tests
    test('Simple Addition', 10, 2, add, 12);
    test('Object First', t(10), 2, add, 12);
    test('Object Second', 10, t(2), add, 12);
    test('Both Objects', t(10), t(2), add, 12);

    // Subtraction Tests
    test('Simple Subtraction', 10, 2, sub, 8);
    test('Object First', t(10), 2, sub, 8);
    test('Object Second', 10, t(2), sub, 8);
    test('Both Objects', t(10), t(2), sub, 8);

    // Power Tests
    test('Simple Power', 10, 2, pow, 100);
    test('Object First', t(10), 2, pow, 100);
    test('Object Second', 10, t(2), pow, 100);
    test('Both Objects', t(10), t(2), pow, 100);

    // Modulo Tests
    test('Simple Modulo', 10, 3, mod, 1);
    test('Object First', t(10), 3, mod, 1);
    test('Object Second', 10, t(3), mod, 1);
    test('Both Objects', t(10), t(3), mod, 1);

    // Positive Functions
    test('Simple positive', 10, null, pos, 10);
    test('Simple positive 2', -10, null, pos, -10);
    test('Object positive', t(10), null, pos, 10);

    // Negative Functions
    test('Simple negative', 10, null, neg, -10);
    test('Simple negative 2', -10, null, neg, 10);
    test('Object negative', t(10), null, neg, -10);
    
    // Booleans
    test('Simple Boolean', true, null, bool, true);
    test('Simple Boolean 2', false, null, bool, false);
    test('Simple Boolean 3', 1, null, bool, true);
    test('Simple Boolean 4', 0, null, bool, false);
    test('Simple Boolean 4', -1, null, bool, true);
    test('Simple Boolean 4', null, null, bool, false);
    test('Simple Boolean 4', undefined, null, bool, false);
    test('Object Boolean', t(0), null, bool, false);
    test('Object Boolean 2', t(1), null, bool, true);
}
