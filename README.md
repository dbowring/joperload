joperload
---------------
**J**avascript **Op**erator Ov**erload**ing

Author: [Daniel Bowring]

Copyright: [Daniel Bowring] 2012

Although not *true* operator overloading, provides a function based approach to operator overloading in JavaScript. If you've used pythons magic functions (like \_\_add\_\_) you'll find this pattern easy to use.

To "overload" an operator, define an instance method on an object with the correct "magic" name. The magic names are the short names of the operators prefixed and suffixed with a double underscore. The function should take one argument for binary operators (add, sub, mult, ...) and no arguments for unary operators (pos, neg). The function then does one of three things:

* If the value is operatable, return the new value (the result of the operation)
* If the value is valid, but non-sensical (think deviding by zero) raise an exception
* If the given type can't be handled, return undefined.

All binary operators can be defined as being forward (div(x,y) -> x,y) or reversed (rdiv(x,y) -> y/x). This allows for the functionality to exist regardless of which of the values comes first. Given op(x,y), first x.\_\_op\_\_(y) will be attempted, then y.\_\_rop\_\_(x), then __op__(x,y) which the the native JavaScript implementation.

In the case of the boolean operators (eq, neq, gt, le, ...), defining one method will allow for its inverse method to be defined. For example, defining \_\_eq\_\_ implictly defines \_\_neq\_\_ and defining \_\_gt\_\_ implicitly defines \_\_nlte\_\_.

  [Daniel Bowring]: http://utahraptor.info/
  [source code]: https://bitbucket.org/dbowring/joperload/overview
