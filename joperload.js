/* ****************************************************************************
* Javascript Operator Overloading Implementation
* Not really overloading, you use global methods, but allows for 
* interactions to be defined
*******************************************************************************
* Features:
*   * Will always fall back onto default javascript operators if there is
*       no custom functionality defined
*   * Intelligent solving
*       * For example, if __eq__ is defined but __neq__ is not, __neq__ will
*           simply be !(__eq__) (or vice-versa)
*
*
* Usage:
*   Instead of using the default operators, where you would like advanced 
*   interactions, use the appropriate function instead.
*       x + y -> add(x, y)
*       x - y -> sub(x, y)
*       x * y -> mult(x, y)
*       x / y -> div(x, y)
*       x % y -> mod(x, y)
*       x == y -> eq(x, y)
*       x != y -> neq(x, y)
*       x === y -> is(x, y)
*       x !== y -> nis(x, y)
*       x > y -> gt(x, y)
*       x >= y -> gte(x, y)
*       x < y -> lt(x, y)
*       x <= y -> lte(x, y)
*       Math.pow(x, y) -> pow(x, y)
*   To define the interactions, define a function that has the same name as the
*   operator function, but with a double slash as a prefix and suffix, that
*   takes one argument - the other object
*
* TODO:
*   * Add unary operators (positive [pos] and negative [neg])
**************************************************************************** */
// Equality
function __eq__(x,y) { return x == y; } // EQual
function __req__(x,y) { return y == x; } // Reverse EQual
function __neq__(x,y) { return x != y; } // Not EQual
function __rneq__(x,y) { return y != x; } // Reverse Not EQual
function __is__(x,y) { return x === y; } // IS
function __ris__(x,y) { return y === x; } // Reverse IS
function __rnis__(x,y) { return y !== x; } // Not IS

// Comparisons
function __gt__(x,y) { return x > y; } // Greater Than
function __rgt__(x,y) { return y > x; } // Reverse Greater Than
function __gte__(x,y) { return x >= y; } // Greater Than or Equal to
function __rgte__(x,y) { return y >= x; } // Reverse Greater Than or Equal to
function __lt__(x,y) { return x < y; } // Less Than
function __rlt__(x,y) { return y < x; } // ReverseLess Than
function __lte__(x,y) { return x <= y; } // Less Than or Equal to
function __rlte__(x,y) { return y <= x; } // ReverseLess Than or Equal to

// Division
function __div__(x,y) { return x/y; } // DIVide
function __rdiv__(x,y) { return y/x; } // Reverse DIVide
// Multiplication
function __mult__(x,y) { return x*y; } // MULTiply
function __rmult__(x,y) { return y*x; } // Reverse MULTiply
// Addition
function __add__(x,y) { return x+y; } // ADD
function __radd__(x,y) { return y+x; } // Reverse ADD
// Subtraction
function __sub__(x,y) { return x-y; } // SUBtract
function __rsub__(x,y) { return y-x; } // Reverse SUBtract
// Power
function __pow__(x,y) { return Math.pow(x,y); } // POWer
function __rpow__(x,y) { return Math.pow(y,x); } // Reverse POWer
// Modulus
function __mod__(x,y) { return x%y; } // MODulo
function __rmod__(x,y) { return y%x; } // Reverse MODulo

// Unary Operators
function __pos__(x) { return +x; } // Positive
function __neg__(x) { return -x; } // Negative
function __bool__(x) { return x ? true : false; } // Boolean

// Iteration
// This should throw an error if the element given cannot become iterable
// (for example, a number). Only valid value at this point are arrays.
function __iter__(x) {
    if (x.constructor == Array) {
        return x;
    }
    throw "Item is not iterable";
}

// Subscript (eg, item[key]) // probably wont support this
// function __getitem__(x,y) { return x[y]; }; // 
// function __setitem__(x,y,v) { return x[y] = v; }

function __r_func__(ltry, rtry, fallback) {
    // Shortcut to make the "reversable" function
    return function(x,y) {
        var value = undefined;
        // Attempt a forward check (x acts on y)
        if (x != undefined && x[ltry]) {
            value = x[ltry](y);
            if (value !== undefined) {
                return value;
            }
        }
        // Attempt a reverse check (y acts on x)
        if (y != undefined && y[rtry]) {
            value = y[rtry](x);
            if (value !== undefined) {
                return value;
            }
        }
        if (fallback) {
            return fallback(x,y);
        }
        return undefined;
    };
}

function __b_func__(ltry, rtry, fallback, iltry, irtry, ifallback) {
    // Shortcut to make boolean comparison functions
    var func = __r_func__(ltry, rtry);
    var ifunc = __r_func__(iltry, irtry);

    return function(x,y) {
        var value = func(x,y);
        if (value != undefined) {
            return value;
        }
        value = ifunc(x,y);
        if (value != undefined) {
            return !value;
        }
        value = fallback(x, y);
        if (value != undefined) {
            return value;
        }
        value = ifallback(x, y);
        if (value != undefined) {
            return value;
        }
        return undefined;
    }
}

function __u_func__(ltry, fallback) {
    return function(x) {
        var value = undefined;
        if (x != undefined && x[ltry]) {
            value = x[ltry]();
            if (value != undefined) {
                return value;
            }
        }
        value = fallback(x);
        if (value != undefined) {
            return value;
        }
        return undefined;
    }
}

eq = __b_func__('__eq__', '__req__', __eq__, '__neq__', '__rneq__', __neq__);
req = __b_func__('__req__', '__eq__', __req__, '__rneq__', '__neq__', __rneq__);
neq = __b_func__('__neq__', '__rneq__', __neq__, '__eq__', '__req__', __eq__);
rneq = __b_func__('__rneq__', '__neq__', __rneq__, '__req__', '__eq__', __req__);

gt = __b_func__('__gt__', '__rgt__', __gt__, '__lte__', '__rlte__', __lte__);
rgt = __b_func__('__rgt__', '__gt__', __rgt__, '__rlte__', '__lte__', __rlte__);
gte = __b_func__('__gte__', '__rgte__', __gte__, '__lt__', '__rlt__', __lt__);
rgte = __b_func__('__rgte__', '__gte__', __rgte__, '__rlt__', '__lt__', __rlt__);
lt = __b_func__('__lt__', '__rlt__', __lt__, '__gte__', '__rgte__', __gte__);
rlt = __b_func__('__rlt__', '__lt__', __rlt__, '__rgte__', '__gte__', __rgte__);
lte = __b_func__('__lte__', '__rlte__', __lte__, '__gt__', '__rgt__', __gt__);
rlte = __b_func__('__rlte__', '__lte__', __rlte__, '__rgt__', '__gt__', __rgt__);

div = __r_func__('__div__', '__rdiv__', __div__);
rdiv = __r_func__('__rdiv__', '__div__', __rdiv__);
mult = __r_func__('__mult__', '__rmult__', __mult__);
rmult = __r_func__('__rmult__', '__mult__', __rmult__);
add = __r_func__('__add__', '__radd__', __add__);
radd = __r_func__('__radd__', '__add__', __radd__);
sub = __r_func__('__sub__', '__rsub__', __sub__);
rsub = __r_func__('__rsub__', '__sub__', __rsub__);
pow = __r_func__('__pow__', '__rpow__', __pow__);
rpow = __r_func__('__rpow__', '__pow__', __rpow__);
mod = __r_func__('__mod__', '__rmod__', __mod__);
rmod = __r_func__('__rmod__', '__mod__', __rmod__);

pos = __u_func__('__pos__', __pos__);
neg = __u_func__('__neg__', __neg__);
bool = __u_func__('__bool__', __bool__);

iter = __u_func__('__iter__', __iter__);
